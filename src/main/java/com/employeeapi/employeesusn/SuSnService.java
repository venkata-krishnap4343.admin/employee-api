package com.employeeapi.employeesusn;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.apache.commons.lang3.StringUtils;

import java.util.ArrayList;

@Service
@Slf4j
@AllArgsConstructor
public class SuSnService implements UserDetailsService {

    SuSnRepository suSnRepository;

    BCryptPasswordEncoder bCryptPasswordEncoder;

    public Employee userSignup(Employee employee){
        EmployeeEntity employeeEntity = suSnRepository.findByEmail(employee.getEmail());
        if(employeeEntity == null || StringUtils.isEmpty(employeeEntity.getEmail())){
            EmployeeEntity savedData = suSnRepository.save(EmployeeEntity.builder()
                    .email(employee.getEmail())
                    .firstName(employee.getFirstName())
                    .lastName(employee.getLastName())
                    .password(bCryptPasswordEncoder.encode(employee.getPassword()))
                    .phoneNumber(employee.getPhoneNumber()).build());
            return  Employee.builder()
                        .email(savedData.getEmail())
                        .firstName(savedData.getFirstName())
                        .lastName(savedData.getLastName())
                        .phoneNumber(savedData.getPhoneNumber())
                        .build();
        }
        return null;

    }

    @Override
    public UserDetails loadUserByUsername(String email) throws UsernameNotFoundException {
        EmployeeEntity employeeEntity = suSnRepository.findByEmail(email);
        if(employeeEntity == null) throw new UsernameNotFoundException(email);
        return new User(employeeEntity.getEmail(), employeeEntity.getPassword(), new ArrayList<>());
    }

    public Employee getUserDetails(String email) {
        EmployeeEntity employeeEntity = suSnRepository.findByEmail(email);

        if(employeeEntity != null){
            return Employee.builder()
                    .email(employeeEntity.getEmail())
                    .firstName(employeeEntity.getFirstName())
                    .lastName(employeeEntity.getLastName())
                    .phoneNumber(employeeEntity.getPhoneNumber())
                    .build();
        }
        return null;
    }
}
