package com.employeeapi.employeesusn;


import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class Employee {
    private String email;
    private String password;
    private String firstName;
    private String lastName;
    private String phoneNumber;
}
