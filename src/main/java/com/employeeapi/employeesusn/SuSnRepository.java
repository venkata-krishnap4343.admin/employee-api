package com.employeeapi.employeesusn;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface SuSnRepository extends JpaRepository<EmployeeEntity, Integer> {
    EmployeeEntity findByEmail(String email);
}
