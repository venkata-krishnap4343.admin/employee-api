package com.employeeapi.employeesusn;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(path = "/employee")
@Slf4j
@AllArgsConstructor
@CrossOrigin(originPatterns = "*")
public class EmployeeSuSnController {

    SuSnService suSnService;

    @PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public Employee userSignUp(@RequestBody Employee employee){
        log.info(" New Sign Up Request for user : {}", employee.getEmail());
        return suSnService.userSignup(employee);
    }

    @GetMapping
    public String status(){
        log.info("Working");
        return "Working";
    }

    @GetMapping(path = "/{email}/get-user")
    public Employee getEmployeeDetails(@PathVariable("email") String email){
        return suSnService.getUserDetails(email);
    }
}
