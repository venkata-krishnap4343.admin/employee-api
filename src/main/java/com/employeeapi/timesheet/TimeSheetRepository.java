package com.employeeapi.timesheet;

import com.employeeapi.employeesusn.EmployeeEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface TimeSheetRepository extends JpaRepository<EmployeeClockInEntity, Integer> {

}
