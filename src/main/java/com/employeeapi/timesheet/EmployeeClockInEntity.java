package com.employeeapi.timesheet;

import com.employeeapi.employeesusn.EmployeeEntity;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.Date;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Entity
@Table(name = "timesheet")
@JsonTypeInfo(use = JsonTypeInfo.Id.CLASS, property = "@class")
public class EmployeeClockInEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;
    private String email;
    private String time;
}
