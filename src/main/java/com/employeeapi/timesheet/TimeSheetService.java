package com.employeeapi.timesheet;

import com.employeeapi.employeesusn.EmployeeEntity;
import com.employeeapi.employeesusn.SuSnRepository;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.tomcat.jni.Time;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.Date;

@Service
@AllArgsConstructor
@Slf4j
public class TimeSheetService {

    TimeSheetRepository timeSheetRepository;
    SuSnRepository suSnRepository;

    public EmployeeClockIn clockIn(EmployeeClockIn employeeClockIn){
        EmployeeEntity employeeData = suSnRepository.findByEmail(employeeClockIn.getEmail());
        if(employeeData!=null) {
            EmployeeClockInEntity savedData = timeSheetRepository.save(EmployeeClockInEntity.builder().email(employeeClockIn.getEmail()).time(LocalDateTime.now().toString()).build());
            return EmployeeClockIn.builder().email(savedData.getEmail()).time(convertToLocalDateTime(savedData.getTime())).build();
        }else{
            throw new RuntimeException("User not found exception");
        }
    }

    private LocalDateTime convertToLocalDateTime(String time) {
        return LocalDateTime.parse(time);
    }

}
