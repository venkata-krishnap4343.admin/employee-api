package com.employeeapi.timesheet;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(path = "employee/timesheet")
@Slf4j
@AllArgsConstructor
public class TimeSheetController {

    TimeSheetService timeSheetService;

    @PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public EmployeeClockIn employeeClockIn(@RequestBody EmployeeClockIn employeeClockIn){
        return timeSheetService.clockIn(employeeClockIn);
    }
}
